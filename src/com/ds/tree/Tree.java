package com.ds.tree;

import java.util.LinkedList;
import java.util.Stack;

class Node {
	int value;
	Node left, right;

	Node(int value, Node left, Node right) {
		this.value = value;
		this.left = left;
		this.right = right;
	}
}

public class Tree {
	public static void main(String[] args) {
		Node n = createTree();
		 System.out.println("Height of Tree- "+getHeight(n));
		 System.out.println("Width of Tree- "+calculateWidth(n));
		 System.out.println("Print Tree in Vertical Order");
		 printTreeInVerticalOrder(n);
		 System.out.println("Print Lowest Common Ancestor for Node 10 and 4");
		 findLCA(n, 10, 4);
		 LinkedList<Integer> s = new LinkedList<>();
		 System.out.println("Print all Paths from Root to Leaf Node");
		 getAllPath(n, 15, s);

	}

	/*
	 * Create Tree Mentioned in Image
	 */
	public static Node createTree() {
		Node n_left_left_right_left_left = new Node(10, null, null);

		Node n_left_left_right_right_left_right = new Node(15, null, null);

		Node n_left_left_right_right_left = new Node(14, null, n_left_left_right_right_left_right);

		Node n_left_left_right_left = new Node(9, n_left_left_right_left_left, null);
		Node n_left_left_right_right = new Node(13, n_left_left_right_right_left, null);

		Node n_left_left_right = new Node(7, n_left_left_right_left, n_left_left_right_right);
		Node n_right_right_right = new Node(8, null, null);

		Node n_left_right_right = new Node(11, null, null);
		Node n_right_left_left = new Node(12, null, null);

		Node n_left_left = new Node(3, null, n_left_left_right);
		Node n_left_right = new Node(4, null, n_left_right_right);
		Node n_right_left = new Node(5, n_right_left_left, null);
		Node n_right_right = new Node(6, null, n_right_right_right);

		Node n_left = new Node(1, n_left_left, n_left_right);
		Node n_right = new Node(2, n_right_left, n_right_right);

		Node n = new Node(0, n_left, n_right);
		return n;
	}

/*
* 	Return Height of the Tree
*/
	public static int getHeight(Node root) {
		int leftHeight = 0, rightHeight = 0;
		if (root.left == null && root.right == null) {
			return 0;
		}
		if (root.left != null) {
			leftHeight = getHeight(root.left);
		}
		if (root.right != null) {
			rightHeight = getHeight(root.right);
		}
		if (leftHeight > rightHeight) {
			return leftHeight + 1;
		} else {
			return rightHeight + 1;
		}
	}
/*
 * Return Width of the tree
 */
	public static int calculateWidth(Node root) {
		int leftWidth = getWidth(root.left, true) + 1;
		int rightWidth = getWidth(root.right, false) + 1;
		return leftWidth + rightWidth;
	}

	public static int getWidth(Node root, Boolean dir) {
		int leftWidth = 0, rightWidth = 0;
		if (root.left == null && root.right == null) {
			return 0;
		}
		if (root.left != null) {
			leftWidth = getWidth(root.left, dir);
			if (dir) {
				leftWidth++;
			} else {
				leftWidth--;
			}
		}
		if (root.right != null) {
			rightWidth = getWidth(root.right, dir);
			if (!dir) {
				rightWidth++;
			} else {
				rightWidth--;
			}
		}
		if (leftWidth > rightWidth) {
			return leftWidth;
		} else {
			return rightWidth;
		}
	}

	/*
	 * Print Tree in Vertical Order
	 */
	public static void printTreeInVerticalOrder(Node root) {
		int leftWidth = getWidth(root.left, true) + 1;
		int rightWidth = getWidth(root.right, false) + 1;
		for (int i = leftWidth * -1; i <= rightWidth; i++) {
			getNodeValueOnWidthIndex(root, i, 0);
			System.out.println();
		}
	}
	public static void getNodeValueOnWidthIndex(Node root, int widthIndex, int currentIndex) {
		if (currentIndex == widthIndex) {
			System.out.print(root.value + " ");
		}
		if (root.left != null) {
			getNodeValueOnWidthIndex(root.left, widthIndex, currentIndex - 1);
		}
		if (root.right != null) {
			getNodeValueOnWidthIndex(root.right, widthIndex, currentIndex + 1);
		}
	}

/*
 * Find Lowest Common Ancestor
 */
	public static void findLCA(Node root, int firstValue, int secondValue) {
		Stack<Integer> l1 = new Stack<Integer>();
		Stack<Integer> l2 = new Stack<Integer>();
		findPath(root, firstValue, l1);
		findPath(root, secondValue, l2);
		int lca = root.value;
		while (!l1.isEmpty() && !l2.isEmpty()) {
			if (l1.peek() == l2.peek()) {
				lca = l1.pop();
				l2.pop();
			} else {
				break;
			}
		}
		System.out.println("LCA---" + lca);
	}

	public static boolean findPath(Node root, int value, Stack<Integer> l) {
		if (root.value == value) {
			return true;
		}
		if (root.left != null) {
			if (findPath(root.left, value, l)) {
				l.push(root.left.value);
				return true;
			}
		}
		if (root.right != null) {
			if (findPath(root.right, value, l)) {
				l.push(root.right.value);
				return true;
			}
		}
		return false;
	}
/*
 * Print All Paths from Root to Leaf Node 
 */
	public static void getAllPath(Node root, int sum, LinkedList<Integer> s) {
		s.add(root.value);
		if (root.left != null) {
			getAllPath(root.left, sum, s);
		}
		if (root.right != null) {
			getAllPath(root.right, sum, s);
		}
		if (root.left == null && root.right == null) {
			Integer arr[] = new Integer[s.size()];
			s.toArray(arr);

			for (Integer i : s) {
				System.out.print(i + " ");
			}
			System.out.println();
		}
		s.removeLast();
	}
	
	public void getConsecutiveSum(){

		int arr[] = { 1, 4, 20, 3, 2, 10, 5 };
		int curr_sum = 0, sum = 25;
		LinkedList<Integer> l = new LinkedList<>();
		for (int i = 0; i < arr.length; i++) {
			l.add(arr[i]);
			if (arr[i] + curr_sum > sum) {
				curr_sum -= l.removeFirst();

			}
			if (arr[i] + curr_sum == sum) {
				for (Integer x : l) {
					System.out.print(x + "  ");
				}
				System.out.println();

			}
			if (arr[i] + curr_sum < sum) {
			}
			curr_sum += arr[i];
		}
	}
}
